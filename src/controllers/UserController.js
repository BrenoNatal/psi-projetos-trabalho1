const { response } = require('express');
const { Op } = require("sequelize");
const User = require("../models/User");
const Auth = require("../config/Auth");
const axios = require('axios');
const { validationResult } = require('express-validator');




const index = async (req, res) => {
    try {
        const user = await User.findAll();
        return res.status(200).json({ user });
    } catch (err) {
        return res.status(500).json({ err });
    }
};

const show = async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({ user });

    } catch (err) {
        return res.status(200).json({ user });
    }
};

const create = async (req, res) => {
    try {

        validationResult(req).throw();
        const { password } = req.body;
        const HashSalt = Auth.generatePassword(password);
        const salt = HashSalt.salt;
        const hash = HashSalt.hash;
        const newUser = {
            name: req.body.name,
            email: req.body.email,
            birthday: req.body.birthday,
            username: req.body.username,
            hash: hash,
            salt: salt
        };
        var config = {

        }

        var email = req.body.email;

        axios.get('https://api.hunter.io/v2/email-verifier?email=' + email + '&api_key=31cbd077dcc119959b7272efae24346740b74df7', config)
            .then(async function (response) {
                const resposta = response.data.data.status
                console.log(resposta)
                if (resposta === 'valid') {
                    const user = await User.create(newUser);
                    return res.status(201).json({ message: "Cadastro feito com sucesso!", user: user });
                }

                res.status(500).json({ error: 'Email não é valido' });
            });


    } catch (err) {
        res.status(500).json({ error: err });
    }
};

const update = async (req, res) => {
    const { id } = req.params;
    try {
        validationResult(req).throw();
        const [updated] = await User.update(req.body, { where: { id: id } });
        if (updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Esse usuário não existe.")
    }
};

const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const deleted = await User.destroy({ where: { id: id } });
        if (deleted) {
            return res.status(200).json("Usuário deletado.");
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Esse usuário não existe.");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
}
