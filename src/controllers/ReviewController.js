const { response } = require('express');
const { Op } = require("sequelize");
const Review = require("../models/Review");
const { validationResult } = require('express-validator');

const index = async (req, res) => {
    try {
        const review = await Review.findAll();
        return res.status(200).json({ review });
    } catch (err) {
        return res.status(500).json({ err });
    }
};

const show = async (req, res) => {
    const { id } = req.params;
    try {
        const review = await Review.findByPk(id);
        return res.status(200).json({ review });
    } catch (err) {
        return res.status(200).json({ review });
    }
};


const create = async (req, res) => {
    try {
        validationResult(req).throw();
        const newReview = {
            rate: req.body.rate,
            text: req.body.text,
            movie_name: req.body.movie_name,
            UserId: req.body.UserId,
        };

        const review = await Review.create(newReview);
        return res.status(201).json({ message: "Review feita com sucesso", review: review });
    } catch (err) {
        res.status(500).json({ error: err });
    }
};

const update = async (req, res) => {
    const { id } = req.params;
    try {
        
        validationResult(req).throw();
        const [updated] = await Review.update(req.body, { where: { id: id } });
        if (updated) {
            const review = await Review.findByPk(id);
            return res.status(200).send(review);
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Essa review não existe.")
    }
};

const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const deleted = await Review.destroy({ where: { id: id } });
        if (deleted) {
            return res.status(200).json("Review deletada.");
        }
        throw new Error();
    } catch (err) {
        return res.status(500).json("Essa review não existe.");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
}
