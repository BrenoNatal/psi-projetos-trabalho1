const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Review = sequelize.define('Review', {
    rate: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    text: {
        type: DataTypes.STRING,
        allowNull: false
    },
    movie_name: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Review.associate = function (models) {
    Review.belongsTo(models.User);
};

module.exports = Review;