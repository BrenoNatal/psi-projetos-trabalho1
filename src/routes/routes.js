const { Router } = require('express');
const UserController = require('../controllers/UserController');
const ReviewController = require('../controllers/ReviewController');
const validator = require("../config/Validator");

const router = Router();


//Autenticação
const AuthController = require("../controllers/AuthController");
const passport = require("passport");
router.use("/private", passport.authenticate('jwt', { session: false }));
router.post("/login", AuthController.login);
router.get("/private/getDetails", AuthController.getDetails);


//Rotas user
router.get('/users',UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users',validator.validationUser('create'), UserController.create);
router.put('/users/:id',validator.validationUser('update'), UserController.update);
router.delete('/users/:id', UserController.destroy);


//Rotas review
router.get('/review', ReviewController.index);
router.get('/review/:id', ReviewController.show);
router.post('/review',validator.validationReview('create'), ReviewController.create);
router.put('/review/:id',validator.validationReview('update'), ReviewController.update);
router.delete('/review/:id', ReviewController.destroy);

module.exports = router;
