# PSI Projetos-Trabalho1 | EJCM

Esse projeto é para o trabalho 1 de tech lead do psi de projetos 23.1

**Status do Projeto** Em desenvolvimento

![Badge](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)
![Badge](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)

## Tabela de Conteúdo

 1. [Tecnologias utilizadas](#tecnologias-utilizadas)
 2. [Instalação](#instalação)
 3. [Configuração](#configuração)
 4. [Uso](#uso)
 5. [Testes](#testes)
 6. [Arquitetura](#arquitetura)
 7. [Autores](#autores)
 

## Tecnologia utilizadas

    -axios : 1.3.4
    -crypto: 1.0.1
    -dotenv: 16.0.3
    -express: 4.18.2
    -express-session: 1.17.3
    -express-validation: 6.15.0
    -jsonwebtoken: 9.0.0
    -node-dev: 8.0.0
    -passport: 0.6.0
    -passport-jwt: 4.0.1
    -sequelize: 6.29.0
    -sqlite3: 5.1.4

    Foi utilizada a api Hunter.io

## Instalação

    Para instalar o projeto começamos com um git clone
    Por exemplo: 

``` bash
$ git clone https://gitlab.com/BrenoNatal/psi-projetos-trabalho1.git
```    

    Depois de clonar o repositorio mudadamos no terminal *para pasta do repositorio para fazer isso no terminal usamos o seguinte comando

``` bash
$ cd psi-projetos-trabalho1
```     


    Agora para instalar as dependencias necessarias usamos o seguinte comando
``` bash
$ npm install
```     


## Configuração

## Uso

    Para usar rodar a api precisamos primeiro rodar alguns comandos
``` bash
$ npm run keys
``` 
``` bash
$ cp .env.example .env
```     
``` bash
$ npm run migrate
``` 
``` bash
$ npm start
``` 

## Testes

## Arquitetura

## Autores

* Dev - Breno Natal
